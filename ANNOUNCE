The Wine development release 1.1.2 is now available.

What's new in this release (see below for details):
  - Control panel improvements and new appwiz panel.
  - Restructurations of state handling in Direct3D.
  - Support for timer queue functions.
  - Many MSXML improvements.
  - Several fixes to Solaris support.
  - Various bug fixes.

The source is available from the following locations:

  http://ibiblio.org/pub/linux/system/emulators/wine/wine-1.1.2.tar.bz2
  http://prdownloads.sourceforge.net/wine/wine-1.1.2.tar.bz2

Binary packages for various distributions will be available from:

  http://www.winehq.org/site/download

You will find documentation on http://www.winehq.org/site/documentation

You can also get the current source directly from the git
repository. Check http://www.winehq.org/site/git for details.

Wine is available thanks to the work of many people. See the file
AUTHORS in the distribution for the complete list.

----------------------------------------------------------------

Bugs fixed in 1.1.2:

   1789  msvcrt type conversion problem
   3714  ERMapper 7 won't run because of registry key issues
   4919  Morrowind fails to start with the error "Unknown Stencil Mode Format"
   6650  MSTSC (Remote Desktop) crashes
   7778  Discworld II info window about no CD in drive not drawn
   7788  Morrowind Elder Scrolls 3 crashes very early in game
   7930  C&C 3 slows down after changing graphics settings
   8176  EverQuest 1 terrain textures disappearing
   8604  No videoplayback in spongebob squarepants - whitescreen
   9035  Adobe/Macromedia Studio MX installer fails
   9709  Solid cyan screen when entering character select in EQ
   9855  Red Alert: A Path Beyond crashes with a sound error
  10146  Wine versions >0.9.43 break PokerAce Hud, as the application fails to create a timer during start-up.
  10936  wine cannot switch 32bit to 16bit
  10991  SPlan 6.0 (schematic editor): Status bar is not show completely
  11198  C & C Tiberian Sun reporsts comctl32.dll is an older version on winver higher then 98
  11936  window size is too small in the "vc2008 redist installer" and "Firefox 3" [dogfood]
  11950  TheBat! 3.99.1 show white squares not icons under wine after 0.9.56
  12023  Broken sound in Live For Speed game
  12210  Copy/paste corruption in The Elder Scrolls Construction Set
  12302  Lord of the Rings:  Shadows of Angmar unplayable due to high lag
  12491  PES 2008 crash at launch with a "fail to get video memory" message
  12582  solidworks crash at startup in mshtml
  12718  Typing the username in MSN Messenger 7.5 is really slow
  12722  intel software codecs video not working for thief/system shock
  12783  Allocation of PEB and TEB can overlap dll mappings
  12905  EVE Online light effects and some other objects are not blended correctly when HDR is enabled
  13307  graphical glitches on wzebra board
  13325  Fireworks 8 trial doesn't draw window background initially
  13450  "Race driver : Grid" demo crash in the menu
  13860  Sacrifice Demo: d3d_surface errors fill console
  13991  eDrawings viewer needs gdiplus.GdipCreateBitmapFromResource
  13999  No videoplayback in spongebob squarepants - whitescreen
  14019  systray icon not displayed in dual screen
  14040  Microsoft Virtual Earth 3D beta inner installer aborts when reading registry
  14155  Enterprise Architect - Text on diagrams renders upside down
  14197  white window when starting the patcher
  14225  HTML help: Browsing local help url's fail & local graphic url's not shown.
  14249  Hearts of Iron 2: Armageddon crashes
  14273  Fonts unreadable on TaxAct forms
  14277  SnelStart installer bails out with error box
  14326  CAJViewer 7.0 hangs on startup
  14344  Morrowind crashes when loading a saved game if music is enabled.
  14351  RtlpWaitForCriticalSection times out towards end of tests
  14356  alt.binz does not run anymore
  14393  atoi() implemenation on large integers is wrong
  14429  compile fail in msxml_private.h:94
  14439  Office 2003 Pro install aborts in 1.1.1 with MS error code 1627
  14441  Rose Online Evolution crash with new msxml patch
  14462  Blitzin2: Cursor not visible
  14483  WinVerifyTrustEx doesn't return expected HRESULT for PE images not digitally signed (TRUST_E_NOSIGNATURE)
  14502  iphlpapi missing Icmp* functions
  14523  msiexec hangs when installing msxml3.msi in 1.1.1
  14524  richtext error 'Insurgency-2.1rc1-Full.exe'
  14527  90º rotated text not shown
  14539  RtlIsTextUnicode regression: ideographic space should not be used in check for reversed control chars
  14568  Silence FIXME from CoGetContextToken stub to prevent flooding of trace output when COM+ context is queried from .NET runtime
  14578  Thief, System Shock 2 - video intros hang/crash in the end

----------------------------------------------------------------

Changes since 1.1.1:

Adam Petaccia (11):
      gdiplus: Stub GdipNewPrivateFontCollection.
      gdiplus: Stub GdipDeletePrivateFontCollection.
      gdiplus: Stub GdipPrivateAddFontFile.
      gdiplus: Stub GdipGetFontCollectionFamilyCount.
      gdiplus: Stub GdipGetFontCollectionFamilyList.
      gdiplus: Implement GdipCreateRegion and mark the tests todo_wine instead of skipping over them all.
      gdiplus: Implement GdipDeleteRegion.
      gdiplus: Implement GdipSetEmpty.
      gdiplus: Implement GdipSetInfinite.
      gdiplus: Implement GdipGetRegionDataSize.
      gdiplus: Stub GdipIsStyleAvailable.

Alex Villacís Lasso (5):
      richedit: Shorten EM_AUTOURLDETECT tests.
      richedit: Tests for visibility behavior of richedit scrollbars,  with todo_wine.
      richedit: More tests for visibility behavior of richedit scrollbars, with todo_wine.
      richedit: Tests for WM_SIZE/scrollbar recursion bug, with todo_wine.
      richedit: Do not read actual scrollbar state for scrollbar update, use internal state instead.

Alexander Dorofeyev (13):
      ddraw: Force surfaces without memory flags to video memory on creation.
      ddraw/tests: Add tests for surface caps memory flags.
      quartz: Make Filtermapper aggregatable.
      quartz: Aggregate filtermapper in filtergraph.
      quartz/tests: Test querying IFilterMapper2 from FilterGraph.
      quartz/tests: Add filtermapper aggregation tests.
      quartz/tests: Fix interface leaks in aggregation test.
      quartz: Fix interface leak in FilterGraph2_RemoveFilter.
      quartz/tests: Fix interface leaks in filtergraph test.
      quartz: Fix variant handling in GetFilterInfo.
      quartz: Add missing VariantClear after GetFilterInfo calls.
      quartz: Fix interface leak in FilterGraph2_Render.
      ddraw: Improve IDirect3DDevice7_Load implementation.

Alexander Nicolaysen Sørnes (10):
      wordpad: Show error message if saving fails.
      regedit: Fix concat handling in unicode import.
      regedit: Add missing fclose on file import.
      notepad: Fix opening Unicode files.
      regedit: Update Norwegian Bokmål translation.
      wordpad: Show error if file open fails.
      winefile: Don't access uninitialized value.
      regedit: Fixes for Norwegian Bokmål translation.
      Updated Norwegian Bokmål Readme file.
      dpnet: Register DirectPlay8ThreadPool object.

Alexandre Julliard (14):
      msxml3: Fix compile without xml headers.
      mstask: Add stubs for the duplicate stdcall entry points.
      icmp: The import library is not needed.
      dbghelp: Always initialize section pointer in ImageDirectoryEntryToDataEx.
      winetest: Fix a compiler warning with the size_t type.
      Avoid exporting common symbols since that's broken on Mac OS X.
      winebuild: Add Solaris as a separate platform.
      winebuild: Don't put the PE header in the .init section on Solaris.
      winegcc: Add support for specifying section alignment on Solaris.
      user32: Fix MapWindowPoints behavior in the process that owns the desktop window.
      user32/tests: Make sure the bitmap info passed to CreateDIBSection contains a valid color table.
      inetmib1: Check for failure of the iphlpapi functions instead of blindly trusting them.
      make_makefiles: Explicitly list the Wine headers that should be exported.
      shell32: Define a valid cursor for the control panel window.

Andrew Talbot (15):
      rpcrt4: Remove unneeded address-of operator from array name.
      setupapi: Remove unneeded address-of operator from array name.
      shlwapi: Remove unneeded address-of operator from array name.
      user32: Remove unneeded address-of operators from array names.
      wined3d: Remove unneeded address-of operators from array names.
      winedos: Remove unneeded address-of operators from array names.
      winejoystick.drv: Remove unneeded address-of operator from array name.
      winex11.drv: Remove unneeded address-of operators from array names.
      wininet: Remove unneeded address-of operators from array names.
      wintrust: Remove unneeded address-of operators from array names.
      ws2_32: Remove unneeded address-of operators from array names.
      shell32: Remove unneeded address-of operators from array names.
      advpack: Remove a useless test.
      advpack: Fix sign-compare warnings.
      advapi32: Sign-compare warnings fix.

Aric Stewart (7):
      comctl32: rebar: Record what changes and only redo the Layout if something of relevance changes.
      comctl32: rebar: Prevent unnecessary triggering of RBBIM_CHILDSIZE changes.
      mlang: Basic implementation of fnIMultiLanguage2_GetCodePageDescription.
      mlang: Stub implementation of IMLangFontLink2.
      mlang: A very basic implementation of the IMLangLineBreakConsole interface and IMLangString interface.
      mlang: Correct and fill out Japanese locale information.
      winex11: Add Japanese Mac keyboard layout as many of the vkey code are quite different.

Aurimas Fischer (1):
      richedit: Spelling fixes.

Cesar Eduardo Barros (1):
      kernel32: Add stub for CreateHardLink.

Dan Hipschman (12):
      libs/wine: Avoid over-allocating memory in default_dbgstr_wn.
      kernel32/tests: Fix p_BindIoCompletionCallback declaration so it compiles with the MS compiler.
      kernel32/tests: Add tests for timer queues.
      kernel32: Add a stub for ChangeTimerQueueTimer.
      kernel32/tests: Add tests for Change/DeleteTimerQueueTimer.
      kernel32/tests: Check that timers are not NULL in timer queue tests.
      ntdll: Implement [Rtl]Create/DeleteTimerQueue[Ex].
      ntdll: Implement RtlCreateTimer for kernel32's CreateTimerQueueTimer.
      include: Add WT_TRANSFER_IMPERSONATION to winnt.h.
      ntdll: Implement the timer queue thread.
      ntdll: Implement RtlUpdateTimer for kernel32's ChangeTimerQueueTimer.
      ntdll: Implement RtlDeleteTimer for kernel32's DeleteTimerQueueTimer.

Dan Kegel (2):
      tools: Allow running tests with valgrind.
      winmm/tests: Test mciSendString with non-null return string buffer.

Daniel Santos (1):
      ws2_32: WSASendTo() should clear last error on success.

David Adam (1):
      d3dx8: Implement D3DXSphereBoundProbe.

Detlef Riekenberg (8):
      winetest: Run tests again on Win9x.
      crypt32/tests: Make the msg tests usable on win9x (avoid crash).
      oleaut32/tests: Fix tmarshal tests on win9x.
      oleaut32/tests: Run more typelib tests on win9x.
      kernel32/tests: Run the tests again on Win9x.
      msxml3/tests: Fix failure and crash on win9x.
      cryptnet/tests: Make the tests usable on win9x.
      appwiz.cpl: Do not access memory after HeapFree in FreeAppInfo.

Dmitry Timoshkov (2):
      gdi32: Relax the gm.gmCellIncX comparison.
      Revert "gdi32: Reselect objects into the DC only if scaling factors change.".

Dylan Smith (4):
      richedit: Enforce the maximum font size.
      richedit: Cannot undo setting of default character format.
      richedit: Fixed regression caused by destroying the caret.
      richedit: Fixed regression that caused endless loop.

Eric Pouech (9):
      winhelp: Pick nicer fonts for buttons.
      winhelp: Fixed memory leak in macro handling.
      winhelp: Implemented SetHelpOnFile macro.
      winhelp: Implemented SetPopupColor macro.
      winhelp: Allow MACRO_Execute to be called recursively.
      winhelp: Move the check about correct RLE decoding inside the decoding routine itself.
      winhelp: Constify the internal .hlp file parsing.
      winhelp: Double clicking in the index list should open the page.
      richedit: Add an assert to point out what we're expecting.

Francois Gouget (9):
      msxml3: Fix compilation on systems that don't support nameless unions.
      winejack: Fix detection of jack libraries with a bad soname.
      wintrust/tests: Fix compilation on systems that don't support nameless unions.
      sane.ds: Use 'sane-config --ldflags'.
      regedit: Remove a couple of left-over debug statements.
      Assorted spelling fixes.
      gphoto2.ds: Make use of `gphoto2-config --libs` for the configure check.
      wineesd: Make use of `esd-config --libs` for the configure check.
      configure: Use cups-config to probe for cups support.

Frans Kool (5):
      shell32: Fixed Dutch translations.
      regedit: Fixed Dutch translations.
      winhlp32: Fixed Dutch translations.
      wordpad: Fixed and added Dutch translations.
      wordpad: Added missing Dutch translations.

Gal Topper (2):
      comdlg32: PrintDlgEx: Change scope of strings for later reuse.
      comdlg32: PrintDlgEx: Add support for RETURNDEFAULT.

Gerald Pfeifer (9):
      comctl32: rebar: Simplify by shedding off unused parameters.
      ntdll/tests: Use NTSTATUS instead of DWORD for status variables.
      comctl32: header: Simplify by shedding off unused parameters.
      comctl32: Fix type of loop variable in HEADER_DeleteItem.
      d3d9: Fix type of loop variable in stream_test() and texop_test().
      avifil32: Make size parameter of AVIFILE_ReadBlock DWORD instead of LONG.
      comctl32: datetime: Simplify by shedding off unused parameters.
      winedbg: Fix the type of four loop variables and reduce scope of one.
      wordpad: Remove unused parameter for preview_command().

H. Verbeet (8):
      wined3d: Improve some shader traces.
      wined3d: Use rev_tex_unit_map instead of assuming there's a 1:1 mapping between samplers and texture units.
      wined3d: Store the texture in a local variable in sampler_texmatrix().
      d3d9: Use color_match() in vshader_version_varying_test().
      d3d9: Replace color_near() with color_match().
      d3d9: Correct some shader comments.
      server: Calling ResumeThread() on a terminated thread is valid.
      d3d9: Use color_match() in test_vshader_input().

Hans Leidekker (10):
      dnsapi: Fix a number of memory leaks.
      snmpapi: Fix a memory leak in the test.
      cabinet: Fix a memory leak.
      setupapi: Fix two memory leaks in the test.
      wininet: Fix a memory leak.
      wininet: Make another test pass on IE6.
      wininet: Delete local file on error in FtpGetFile.
      wininet: Move insertion of cookie header from HttpOpenRequest to HttpSendRequest.
      wininet: Fix cookie buffer overflow.
      usp10: Use a valid string analysis when testing ScriptXtoCP and ScriptCPtoX.

Huw Davies (6):
      user32: Don't read past the end of a global memory block.
      gdiplus: Skip some tests if certain fonts are not installed.
      winmm: Only fill in the MIXERLINEA struct if the call to mixerGetLineInfoW succeeds.
      user32: Don't access pConv after it's been freed.
      gdiplus: Remove incorrect test.
      user32: Don't send the menu clicks if we can't retrieve the item rect.

Hwang YunSong(황윤성) (1):
      winhlp32: Updated Korean resource.

Ismael Barros (2):
      dplayx: Moved dplaysp.h to include/wine.
      dplayx: Basic implementation of dpwsockx.dll, needed by dplayx.

Jacek Caban (13):
      mshtml: Allow timers to be cleaned during processing.
      mshtml: Added IHTMLElement2::getElementsByTagName implementation.
      mshtml: Added IHTMLElement2::getElementsByTagName tests.
      msi: Added Session::Message implementation.
      wininet: Move InternetQueryOption(INTERNET_OPTION_USER_AGENT) to vtbl.
      wininet: Moved more InternetQueryOption implementation to vtbl.
      wininet: Moved InternetQueryOption(INTERNET_OPTION_PROXY) implementation to vtbl.
      wininet: Move remeining InternetQueryOption implementation to vtbl.
      hhctrl.ocx: Fixed ref count handling in IOleClientSiteImpl.
      mshtml: Added IHTMLDocument2::put_title implementation.
      mshtml: Added IHTMLDocument2::get_title implementation.
      mshtml: Added IHTMLDocument2::[get|put]_title tests.
      mshtml: Added IOmNavigator::get_appCodeName implementation.

James Hawkins (24):
      comctl32: Explicity set the number of tics before testing tic placement.
      comctl32: Free the trackbar tics.
      fusion: Allow parsing the blob stream without a #.
      fusion: Load the table row numbers before calculating the table offsets.
      fusion: The VersionLength member is not constant, so dynamically load the metadata header.
      fusion: Add the CorTokenType and use those constants in the assembly code.
      fusion: Add handling for alternate-sized table indices in the metadata.
      fusion: Handle DWORD-sized string indices when loading the assembly name.
      fusion: The cases are tokens, not table indices.
      kernel32: Add tests for GetPrivateProfileString.
      kernel32: Fix removing trailing spaces from lpDefault for GetPrivateProfileString.
      kernel32: Return the default value if lpKeyName is empty.
      services: Assign the service manager db to the lock in LockServiceDatabase.
      msi: Initialize the update function pointer when creating the control.
      kernel32: Don't get the profile string if the buffer length is zero.
      msi: Add more tests for MsiGetSourcePath.
      msi: Test getting the source path from a package with compressed files.
      msi: Test getting the source path from a package with short file names.
      msi: Halt the installation with an error if a source file is missing.
      msi: msidbFileAttributesVital has no effect on the installation of files.
      msi: Test mixing short and long source paths when installing a file.
      msi: Use the newly added defines from the msidbSumInfoSourceType enumeration.
      msi: Set all folders' source paths to the root directory if the source type is compressed.
      msi: Determine the source path based on the short/long file names bit of the Word Count summary property.

John Reiser (3):
      configure: Add check for valgrind/valgrind.h.
      ntdll: When tracking allocated blocks, RtlDestroyHeap must notify that all the blocks are being freed.
      ntdll: Tell valgrind to load the .pdb debugging info for the module that was just loaded.

Jon Griffiths (2):
      msvcrt: Fix exponents in *printf family.
      mswsock: Implement 3 functions by calling through SIO_GET_EXTENSION_FUNCTION_POINTER.

Juan Lang (14):
      crypt32: Add MS root certs to root store.
      crypt32: Don't expect ImageGetCertificateData to succeed when Certificate is NULL.
      wintrust: Move provider function tests to their own function.
      wintrust: Add tests for WinVerifyTrust/WinVerifyTrustEx.
      wintrust: Execute WinVerifyTrust as a sequence of steps, and return the error code from the first failing step of the sequence.
      wintrust: Correct error slot for SoftpubLoadSignature.
      crypt32: Reorganize root store creation code.
      iphlpapi: Move Icmp* functions from icmp to iphlpapi.
      iphlpapi: Partially implement IcmpSendEcho2 using IcmpSendEcho.
      crypt32: Fix GUID output for failing tests by passing buffer to output function.
      crypt32: Test CryptRetrieveSubjectGUID with a cab file.
      crypt32: Add cab file checking to CryptSIPRetrieveSubjectGUID.
      wintrust: Don't assume input file is a PE file in CryptSIPGetSignedDataMsg.
      wintrust: Implement CryptSIPGetSignedDataMsg for cabinet files.

Kai Blin (1):
      secur32: Fix GetComputerObjectName tests for domain members.

Kirill K. Smirnov (1):
      shell32: Avoid usage of undefined xdg_results variable, since XDG_UserDirLookup() always corrupts it.

Lei Zhang (10):
      user32: Add a test for SetWindowPos.
      user32: Clamp newly created windows to the minimum window size.
      oleaut32: Check return values in several functions.
      winex11: X11DRV_DIB_[GS]etImageBits should return 0 on failure.
      gdi32: CreateDIBitmap should check for SetDIBits failure.
      gdiplus: Add tests for passing negative strides to GdipCreateBitmapFromScan0.
      ole32: Silence FIXME from CoGetContextToken stub.
      ntdll: Use our own implementation of atoi and atol.
      gdi32: Add a test for font orientation.
      gdi32: Glyph advances should be measured for unrotated characters.

Luis Busquets (4):
      d3dx9: Implement D3DXGetShaderSize().
      d3dx9: Add a test for D3DXGetShaderSize().
      d3dx9: Implement D3DXGetShaderVersion().
      d3dx9: Add a test for D3DXGetShaderVersion().

Maarten Lankhorst (11):
      quartz: Fix incorrect use of mtCurrent in transform filter.
      quartz: Fix memory and sample leaks.
      quartz: Only allocate 1 buffer in transform filter.
      quartz: Fix reference leak in avi splitter on end of stream.
      winemp3: Be more tolerant to what a valid mp3 header is.
      quartz: Explicitly handle wave headers in avi splitter.
      quartz: Fix wrong assignment of variable in parser.
      include: Fix typo in vmrender.idl.
      quartz: Fix handling of zero byte samples and endofstreams.
      quartz: Fix bugs that occur during connection.
      quartz: Fix end of stream handling in avi splitter.

Mathias Kosch (2):
      winex11: Fix typo in X11DRV_SetDIBits optimization.
      advapi32: Fix RegGetValue when dwFlags includes RRF_RT_ANY.

Michael Stefaniuc (1):
      msxml3: Remove redundant NULL check before HeapFree (Smatch).

Mikołaj Zalewski (16):
      comctl32: statusbar: Window is always Unicode, CCM_SETUNICODEFORMAT should change the notify format.
      comctl32: statusbar: Rename NtfUnicode to bUnicode to make is more consistent with other controls, use NF_REQUERY in WM_CREATE.
      comctl32: statusbar: WM_CREATE should not resize the window - it should happen in WM_SIZE.
      comctl32: statusbar: Don't issue a WM_PAINT during a WM_CREATE.
      comctl32: statusbar: WM_SETFONT should change the bar height.
      comctl32: Fix the file version string declaration.
      oleaut32: Use custom file version in VERSIONINFO.
      comctl32: statusbar: Change the height expression to one that seems to be the same as in Windows.
      comctl32: Don't print ERRs for reflected messages.
      comctl32: toolbar: TB_SETBITMAPSIZE should not change a coordinate when passed -1 (with testcase).
      comctl32: statusbar: Test and fix SB_SETMINHEIGHT.
      comctl32: statusbar: Optimize STATUSBAR_RefreshPart.
      shell32: autocomplete: Don't pass NULL pceltFetched to IEnumString_Next.
      include: Add KEY_WOW64_* defines.
      advapi32: Remove access checks from advapi32 (makes KEY_WOW64_* work).
      advapi32: tests: Make WOW64 test pass under Win2k, fix a flag.

Nicolas Le Cam (2):
      comctl32/tests: Fix a test failure in multiple platforms.
      kernel32/tests: Fix a failing test in Vista.

Nikolay Sivov (8):
      gdiplus: Implemented GdipCreateBitmapFromResource.
      gdiplus: Fix GdipCreatePathIter to handle NULL as path. Fix tests.
      gdiplus: Fix for GdipPathIterNextMarker to handle path without markers. Fix tests.
      gdiplus: Make GdipInvertMatrix test pass on native switching to matrix with determinant 16.
      gdiplus: implemeted GdipGetPathGradientBlendCount with test.
      gdiplus: Implement GdipGetPathGradientBlend with basic tests.
      gdiplus: Implemented GdipGetPathGradientRect with test.
      gdiplus: Fix swapped got-expected in brush test.

Owen Rudge (18):
      shell32: Remove "no control panels" found message.
      shell32: Use wide functions when creating control panel.
      shell32: Load Control Panel title from resources.
      shell32: Add menu bar to Control Panel.
      shell32: Add code to process menu item selections.
      shell32: Add About dialog to Control Panel.
      shell32: Remove existing painting methods from control panel.
      shell32: Use a listview for the control panel.
      shell32: Add status bar to control panel.
      appwiz.cpl: Add skeleton Add/Remove Programs control panel.
      appwiz.cpl: Display Add/Remove Programs dialog.
      appwiz.cpl: Add column headers to listview.
      appwiz.cpl: Add imagelist for listview.
      appwiz.cpl: Check to see if buttons should be enabled.
      appwiz.cpl: Read installed application information from registry.
      appwiz.cpl: Add applications to list, remove on window close.
      appwiz.cpl: Add uninstall routine for applications.
      appwiz.cpl: Add basic Support Information dialog.

Philip Nilsson (4):
      wined3d: Enable filtering for D3DFMT_A4R4G4B4.
      d3dx8: Allow input and output parameters pointing to the same place.
      d3dx9: Implement D3DXPlaneTransformArray.
      d3dx9: Use UINT instead of unsigned int where appropriate.

Piotr Caban (25):
      msxml3: Fixed typo in create_bsc.
      msxml3: Added stub implementation of ISAXLocator.
      msxml3: Added partial implementation of ISAXXMLReader_parse.
      msxml3: Added ISAXContentHandler_startDocument event.
      msxml3: Added ISAXLocator_getLineNumber and ISAXLocator_getColumnNumber partial implementation.
      msxml3: Added ISAXContentHandler_endDocument event.
      msxml3: Added ISAXContentHandler_startElement event.
      msxml3: Added ISAXContentHandler_endElement event.
      msxml3: Added ISAXContentHandler_characters event.
      msxml3: Added ISAXContentHandler_putDocumentLocator event.
      msxml3: Added ISAXLocator_getPublicId.
      msxml3: Added ISAXLocator_getSystemId.
      msxml3/tests: Added more tests for ISAXXMLReader_parse and ISAXLocator.
      msxml3: Do not invoke ISAXXMLReader_endDocument when parser was stopped.
      msxml3/tests: Added tests for ISAXXMLReader_putErrorHandler and ISAXXMLReader_getErrorHandler.
      msxml3: Add error handling.
      msxml3: Added support for SafeArrays in ISAXXMLReader_parse.
      msxml3/tests: Added test for ISAXXMLReader_parse with SafeArray as argument.
      msxml3: Reorganize ISAXXMLReader_parse function.
      msxml3: Added support for more arguments in ISAXXMLReader_parse.
      msxml3: Change ISAXXMLReader_getLine and ISAXXMLReader_getColumn functions.
      msxml3: Change ISAXXMLReader_characters implementation so it works correctly on files with '\r' characters.
      msxml3/tests: Add test for parsing from IStream.
      msxml3/tests: Add test for parsing document with '\r' characters.
      msxml3: Fix for accessing uninitialized memory.

Reece Dunn (13):
      uxtheme/tests: Use HRESULT_FROM_WIN32 to check the error codes, not LOWORD.
      shell32: Do not use a fixed path in the shelllink tests.
      shell32: Make the tests build with the Vista SDK.
      oleaut32: Fix the vartype tests on Vista.
      shell32/tests: Vista returns E_INVALIDARG if the path sent to SetPath is invalid.
      comctl32: Fixed the MRU tests on Vista.
      setupapi/tests: Use FIELD_OFFSET instead of offsetof.
      user32: Fixed building the tests with the Vista SDK.
      shell32: Don't crash if the shlexec tests fail to create an extension in the registry.
      crypt32/tests: Be more verbose on the failing base64 tests on Vista to help locate the failures.
      comctl32: Made the button theme drawing code extensible.
      crypt32/tests: fix the protectdata tests on Vista.
      oleaut32: Fix the SafeArrayGetVartype tests on Vista and use that behaviour.

Rob Shearman (6):
      rpcrt4: Implement MesEncodeIncrementalHandleCreate, MesDecodeIncrementalHandleCreate, MesIncrementalHandleReset and MesHandleFree.
      rpcrt4: Implement NdrMesProcEncodeDecode.
      rpcrt4: Implement MesDecodeBufferHandleCreate and MesEncodeFixedBufferHandleCreate.
      rpcrt4: Ignore the return value of functions when unmarshalling a pickled procedure.
      rpcrt4: Implement RpcCancelThreadEx.
      rpcrt4: Fix the memory pointer passed into the conformant array marshaller in NdrComplexStructMarshall.

Roy Shea (4):
      mstask: Skeleton implementation of Task Scheduler Service.
      mstask: Generate C file with GUID definitions from mstask.idl.
      mstask: TaskScheduler stub with AddRef, QueryInterface, and Release.
      mstask: Implement ClassFactory, DllGetClassObject, and DllCanUnloadNow.

Stefan Dösinger (41):
      wined3d: Remove the atifs shader backend.
      wined3d: NP2 coordinate adjustment is a vertex operation.
      wined3d: Move fixed function texture dimension updates to the fragment pipeline.
      wined3d: Simplify the fragment pipeline selection.
      wined3d: Give the nvts/nvrc code its own pipeline implementation.
      wined3d: WINED3DTSS_TEXCOORDINDEX belongs to the vertex pipeline.
      wined3d: Move shader constant affecting states to the misc pipeline.
      wined3d: Give nv* fixed function stuff its own state template.
      wined3d: Separate texture_activate_dimensions.
      wined3d: Remove some dead code.
      wined3d: Add extension information to the states.
      wined3d: Coalesce the nvrc and nvts+nvrc state template.
      wined3d: Move an extension check into the state template.
      wined3d: Move a few extension checks into the state template.
      wined3d: Move the range fog support test to the template.
      wined3d: Split the psizemin handler.
      wined3d: Split the psizemax state handler.
      wined3d: Move the point sprite support test into the template.
      wined3d: Test for multisampling in the template.
      wined3d: Only register the np2 texture fixup if needed.
      wined3d: Check for ATI_ENVMAP_BUMPMAP in the template.
      wined3d: Move the vertexblend support check to the template.
      wined3d: Remove GL_EXT_vertex_weighting stubs.
      wined3d: Remove more dead code.
      wined3d: Move an VBO support check to the state template.
      d3d9: Unset the vertex shader in the compare_instructions test.
      wined3d: Fix the MULTIPLYADD parameter orders in atifs.
      wined3d: Correct the D3DTOP_LERP parameter order.
      wined3d: Use GL_ARB_texture_non_power_of_two emulation.
      wined3d: Use the nofilter mipmap lookup for np2 textures.
      wined3d: Use less strict pixel format matching if the match fails.
      wined3d: Fix a copy and paste bug.
      wined3d: Whitespace fix.
      wined3d: Set WINED3DDEVCAPS2_VERTEXELEMENTSCANSHARESTREAMOFFSET.
      wined3d: Remove some #ifdefs.
      wined3d: Move set_tex_op(_nvrc) to their specific files.
      d3d: D3DDECLTYPE_UNUSED is not valid in vertex declarations.
      wined3d: Honor the texcoordindex when setting the texture matrix.
      wined3d: Support ATI's D3DFMT_ATI2N format.
      wined3d: ATI2N support using GL_EXT_texture_compression_rgtc.
      wined3d: Make the MAC ARBvp implementation happy about ARL.

Stefan Leichter (1):
      rasapi32: RasEnumDevicesA test: initialize the input parameters before each call, make sure the return buffer is big enough.

Ulrich Hecht (1):
      dsound: Fix notification order.

Vincent Povirk (3):
      wininet: Always respect Proxy-Connection: or Connection: if it exists.
      ole32: Add tests for OleIsCurrentClipboard(NULL).
      ole32: Always return false when asked if NULL is the current clipboard.

Vladimir Pankratov (1):
      winhlp32: Update Russian translation.

Zac Brown (3):
      ntdll: Remove byte reversed U+3000 (CJK space) from being checked in RtlIsTextUnicode.
      winhttp/tests: Add test for opening request.
      include/winhttp.h: Add remaining flags.

Zhangrong Huang (2):
      msxml3: Add support for accessing document node.
      msxml3: Add support for accessing CDATASection node.

--
Alexandre Julliard
julliard@winehq.org
